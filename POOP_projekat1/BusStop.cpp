#include "BusStop.h"
#include "Line.h"
using namespace std;

void BusStop::deleteLineFromSet(Line*l) {
	for (auto i = setLines.begin(); i != setLines.end(); ) {
		if ((*i) == l) {
			setLines.erase(i);
			break;
		}
		else
			i++;
	}
}

void BusStop::insertLineInSet(Line *l) {
	setLines.insert(l);
}

set<Line*> BusStop::linesThroughStop() {
	return this->setLines;
}

set<BusStop*> BusStop::drive1stop() {
	set<BusStop*> s;
	for_each(setLines.begin(), setLines.end(), [this,&s](Line* line) {
		BusStop * stop=line->getDirA().findNextStop(this->getNumber());
		if (stop == nullptr) stop = line->getDirB().findNextStop(this->getNumber());
		if((stop!=nullptr)&&(s.find(stop)==s.end())) s.insert(stop);
	});
	return s;
}

int BusStop::minNumOfTransfer(BusStop* s1, BusStop* s2) {
	BusStop *next;
	int sifra, sifraStajalista;
	set<BusStop*> skup, pretskup;
	int i = 0;
	skup.insert(s1);
	pretskup = skup;
	while (true) {
		for (auto stajaliste : pretskup) {
			sifra = stajaliste->getNumber();

			for (auto lin : stajaliste->getSetLines()) {
				char smer = 'A';
				while (smer != 'C') {
					next = lin->getDir(smer).findNextStop(stajaliste->getNumber());
					while (next != nullptr && next->getNumber() != sifra) {	
						skup.insert(next);
						BusStop *st = next;
						sifraStajalista = next->getNumber();
						if (sifraStajalista == s2->getNumber())						
							return i;
						next = lin->getDir(smer).findNextStop(st->getNumber());
					}
					++smer;
				}
			}
		}
		if (pretskup == skup)
			return -1;		
		++i;
		pretskup = skup;
	}
}


bool BusStop::insertInSet(set<Elem>& set1 , set<Elem>& set2, BusStop*s1, BusStop*s2, map<int, int> &m) {
	for (Elem i : set1) {
		for (BusStop* j : i.stop->drive1stop()) {
			if (m[j->getNumber()] == 1) continue;
			vector<BusStop*>v;
			for (BusStop*k : i.path) v.push_back(k);
			v.push_back(i.stop);
			set2.insert(Elem(j,v));
			m[j->getNumber()] = 1;
			if (j == s2) return true;
		}
	}
	set1.clear();
	return false;
}

vector<BusStop*> BusStop::shortestPath(BusStop* s1, BusStop* s2) {
	set<Elem> set1, set2;
	map<int, int> m;
	vector<BusStop*> v;
	v.clear();
	set1.insert(Elem(s1,v));
	m[s1->getNumber()] = 1;

	while (true){
		if (insertInSet(set1, set2, s1, s2,m)) {
			for (Elem i : set2)
				if (i.stop == s2) {
					i.path.push_back(s2); return i.path;
				}
		}
		if (insertInSet(set2, set1, s1, s2,m)) {
			for (Elem i : set1)
				if (i.stop == s2) {
					i.path.push_back(s2); return i.path;
				}
		}
		if (set1.empty() && set2.empty()) break;
	}
	return v;
}