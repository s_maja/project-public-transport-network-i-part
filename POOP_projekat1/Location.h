#ifndef _LOCATION_H_
#define _LOCATION_H_

#include <iostream>
using namespace std;

class Location{
private:
	double x, y;
public:
	Location(double xx, double yy) {
		x = xx; 
		y = yy;
	}

	double getX() const { return x; }
	double getY() const { return y; }

	double d(Location& t) { return sqrt(pow(x - t.x, 2) + pow(y - t.y, 2)); }

	friend ostream& operator<<(ostream& ot, const Location& loc) {
		return ot << "(" << loc.x << ", " << loc.y << ")";
	}
};

#endif // !_LOCATION_H_
