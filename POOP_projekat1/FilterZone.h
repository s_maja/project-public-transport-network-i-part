#ifndef _FILTER_ZONE_H_
#define _FILTER_ZONE_H_

#include "Filter.h"

class FilterZone : public Filter {
public:
	FilterZone(Network* n): Filter(n) {}

	void filtring(int zone, int parametar2 = -1) {
		for(auto i= network->getListLines().begin();i!= network->getListLines().end();){
			bool boo = false;
			for (auto iter = (*i)->getDirA().getBusStops().begin(); iter != (*i)->getDirA().getBusStops().end();iter++) 
				if ((*iter)->getZone() > zone) {
					auto& temp = i++;
					network->deleteLine((*temp));
					boo = true;
					break;
				}
				
			if (boo == true) continue;
			
			for (auto iter = (*i)->getDirB().getBusStops().begin(); iter != (*i)->getDirB().getBusStops().end();iter++)
				if ((*iter)->getZone() > zone) {
					auto& temp = i++;
					network->deleteLine((*temp));
					break;;
				}
			if (boo == true) continue;
			i++;
		}
	}
};
#endif // !_FILTER_ZONE_H_
