#ifndef _GENERATOR_H_
#define _GENERATOR_H_

#include "Network.h"

class Generator{
protected:
	char mark;
	Network* network;
public:
	Generator(Network * n) : network(n) {}
	virtual ~Generator(){}

	Network* getNetwork() { return network; }
	virtual void generate() = 0;
	virtual char getMark() = 0;

	virtual set<Line*>& getNodes() = 0; //C_generator
	virtual set<pair<Line, Line>>& getEdges() = 0;
	virtual set<BusStop*>& getNodes(int) = 0; //L_generator
	virtual set<pair<BusStop*, BusStop*>>& getEdges(int) = 0;
	
};
#endif // !_GENERATOR_H_
