#include "CSV_NeighLists.h"

void CSV_NeighLists::filtringL() {
	ofstream myfile;
	myfile.open(fileName);
	for (BusStop* i : gen->getNodes(1)) {
		myfile << i->getNumber();

		for (pair<BusStop*, BusStop*> j : gen->getEdges(1))
			if (j.first->getNumber() == i->getNumber())
				myfile << ";" << j.second->getNumber();
		myfile << endl;
	}
	myfile.close();
}
 void CSV_NeighLists::filtringC() {
	 ofstream myfile;
	 myfile.open(fileName);
	 map<Line,set<Line>> m;
	 for (pair<Line, Line> j : gen->getEdges())
		 m[j.first].insert(j.second);

	 for (Line* i : gen->getNodes()) {
		 myfile << i->getNumber();

		 set<Line> s = m[*i];
		 for(auto j: s)
			myfile << ";" << j.getNumber();
		 myfile << endl;
	 }
	 myfile.close();
}