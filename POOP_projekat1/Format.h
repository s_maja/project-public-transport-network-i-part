#ifndef _FORMAT_H_
#define _FORMAT_H_

#include "Generator.h"
#include "CGenerator.h"
#include "LGenerator.h"

class Format {
protected:
	string fileName;
	Generator *gen = nullptr;
public:
	Format() = default;
	virtual ~Format() { if (gen) delete gen; }
	Format(string name, Generator* g=nullptr): gen(g), fileName(name) {}
	
	virtual void formating() = 0;
};
#endif // !_FORMAT_H_
