#ifndef _DIRECTION_H
#define _DIRECTION_H

#include "BusStop.h"

class Direction {
private:
	list<BusStop*> busStops;
	static map<int, BusStop*> mapOfStops;
public:
	Direction() = default;
	~Direction();

	const list<BusStop*>& getBusStops() const { return busStops; }
	list<BusStop*>& getBusStops() { return busStops; }
	static map<int, BusStop*>& getMap()  { return mapOfStops; }

	void addStop(int,string , Location, int, Line*);
	void readAllStops(string, Line*);
	void insertStop(BusStop,Line*);
	void removeStop(BusStop* stop) { busStops.remove(stop);  }
	BusStop* findStop(int);
	BusStop* findNextStop(int);
	void deleteBusStops() { busStops.clear(); }
	void deleteLineFromStops(Line *);
	void deleteBusStop(int);

	friend ostream& operator<<(ostream& ot, const Direction& dir);

};
#endif // !_DIRECTION_H
