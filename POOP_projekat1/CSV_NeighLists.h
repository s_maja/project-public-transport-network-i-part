#ifndef _CSV_NEIGHLISTS_H_
#define _CSV_NEIGHLISTS_H_

#include "CSVFormat.h"

class CSV_NeighLists : public CSVFormat {
public:
	CSV_NeighLists() = default;
	CSV_NeighLists(string name, Generator* g) : CSVFormat(name, g) {}

	virtual void filtringL() override;
	virtual void filtringC()override;
};

#endif // !_CSV_NEIGHLISTS_H_
