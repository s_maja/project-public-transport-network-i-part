#ifndef _CSV_EDGES_H_
#define _CSV_EDGES_H_

#include "CSVFormat.h"

class CSV_Edges: public CSVFormat {
public:
	CSV_Edges() = default;
	CSV_Edges(string name, Generator* g) : CSVFormat(name, g) {}

	virtual void filtringL() override;
	virtual void filtringC()override;
};


#endif // !_CSV_EDGES_H_
