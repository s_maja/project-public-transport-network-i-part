#ifndef _NETWORK_H_
#define _NETWORK_H_

#include "Line.h"

class Network {
private:
	list<Line*> lines;
	string name;
public:
	Network(string n) :name(n){}
	~Network();

	string getName()const { return name; }
	const list<Line*>& getListLines() const { return lines; }
	list<Line*>& getListLines()  { return lines; }

	void readAllLines(string);  //group data loading
	bool readLine(string, string);  //data loading for certain line
	Line* findLine(string); // find line with name
	

	Network insertLine(Line*); //insert new line
	void deleteLine(Line*);

	map<pair<Line, Line>, int> allPairWithCommonStops(int n=-1); //find num of common stops for each pair of lines with common stop
	const BusStop* nearestStop(Location&, Line* l=nullptr); //finding nearest stop for given stop
	
	friend ostream& operator<<(ostream& ot, const Network& net);
};

#endif