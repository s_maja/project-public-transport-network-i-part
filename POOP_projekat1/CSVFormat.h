#ifndef _CSVFORMAT_H_
#define _CSVFORMAT_H_

#include "Format.h"

class CSVFormat:public Format {
public:
	CSVFormat() = default;
	CSVFormat(string name, Generator* g) : Format(name, g) {}

	virtual void filtringL() = 0;
	virtual void filtringC() = 0;

	void formating() override {
		(gen->getMark() == 'L') ? filtringL() : filtringC();
	}
};
#endif // !_CSVFORMAT_H_
