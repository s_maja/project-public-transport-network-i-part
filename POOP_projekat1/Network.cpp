#include "Network.h"

void Network::readAllLines(string fileName)
{
	ifstream file(fileName);
	string s;

	regex reg("([^!]*)!([^!]*)!([^!]*)!");

	while (getline(file, s)) {
		smatch result;
		if (regex_match(s, result, reg)) {

			string num = result.str(1); //number of line
			if (findLine(num) != nullptr)continue;
			string fStop = result.str(2); //first stop
			string lStop = result.str(3); //last stop

			Line * l = new Line(num, fStop, lStop);
			insertLine(l);  //insertLine

			string fileName1 = num + "_dirA.txt";
			lines.back()->getDirA().readAllStops(fileName1,l); //insert stops dirA
			string fileName2 = num + "_dirB.txt";
			lines.back()->getDirB().readAllStops(fileName2,l); //insert stops dirB
		}
		else
		{
			cout << "No match" << endl;
		}
	}
	file.close();
}

bool Network::readLine(string fileName, string numLine)
{
	if (findLine(numLine) != nullptr) return false;
	ifstream file(fileName);
	string s;

	regex reg("([^!]*)!([^!]*)!([^!]*)!");

	while (getline(file, s)) {
		smatch result;
		if (regex_match(s, result, reg)) {

			string num = result.str(1); //number of line
			if (num != numLine) continue;
			string fStop = result.str(2); //first stop
			string lStop = result.str(3); //last stop

			Line *l = new Line(num, fStop, lStop);
			insertLine(l);  //insertLine

			string fileName1 = num + "_dirA.txt";
			lines.back()->getDirA().readAllStops(fileName1,l); //insert stops dirA
			string fileName2 = num + "_dirB.txt";
			lines.back()->getDirB().readAllStops(fileName2,l); //insert stops dirB
			break;
		}
		else
		{
			cout << "No match" << endl;
		}
	}
	file.close();
	return true;
}

Line* Network::findLine(string n){
	for (auto& i : lines) {
		if (i->getNumber() == n)
			return i;
	}
	return nullptr;
}

Network Network::insertLine(Line *l)
	{
		lines.push_back(l);
		return *this;
	}

map<pair<Line, Line>, int> Network::allPairWithCommonStops(int n) {
	map<pair<Line, Line>, int> m;

	for (Line * i: lines) {
		map<Line, int> s= i->numCommonStopsWithLines();
		auto j = s.begin();
		for (; j != s.end(); ++j) {
			if (i->getNumber() >= j->first.getNumber()) continue;
			pair<Line, Line> p = make_pair(*i, j->first);
			if (m.find(p) == m.end())
				m[p] = j->second;
		}
	}

	if (n < 0) return m;
	for (auto iter = m.begin(); iter != m.end();) {
		if (iter->second < n)
			iter = m.erase(iter);
		else
			iter++;
	}
	return m;
}

ostream & operator<<(ostream & ot, const Network & net)
	{
		for (auto const& i : net.lines) {
			ot << *i;
		}
		return ot;
	}

void Network::deleteLine(Line* l) {

	for (auto tek = lines.begin(); tek != lines.end();) {
		if (l->getNumber() == (*tek)->getNumber()) {
			(*tek)->getDirA().deleteLineFromStops(l);
			(*tek)->getDirB().deleteLineFromStops(l);
			delete *tek;
			lines.erase(tek);
			break;
		}
		else
			tek++;
	}
}

const BusStop* Network::nearestStop(Location& loc,Line* l) {
	BusStop *bs=nullptr;  //take a first stop from a first line
	double d = DBL_MAX;

	for_each(lines.begin(), lines.end(),
		[&l, &d, &bs, &loc](Line* t1) {
		if ((l != nullptr)&&(l!=t1)) return;
		int dir = 0;
		Direction dd;
		while(dir!=2){
		dir ? (dd = t1->getDirA()) : (dd = t1->getDirB());
		for_each(dd.getBusStops().begin(), dd.getBusStops().end(),
			[&](BusStop* t2) {
			if (t2->getLocation().d(loc) < d) {
				d = t2->getLocation().d(loc);
				bs = t2;
			}
		});
		dir++;
	}
});

	return bs;
}

Network::~Network() {
	for (auto iter = lines.begin(); iter != lines.end();) {
		auto& temp = iter++;
		lines.erase(temp);
	}
}