﻿#include "FilterZone.h"
#include "FilterNumStops.h"
#include "FilterNumLine.h"
#include "GMLFormat.h"
#include "CSV_Edges.h"
#include "CSV_NeighLists.h"
#include <thread>     
#include <chrono> 

void main() {
	system("color 8B");
	Network* network = new Network("BusPlus");
	LGenerator *gL= new LGenerator(network);
	CGenerator *gC= new CGenerator(network);
	Format *f = nullptr;
	while (true) {
		int i;
		string s,s1;
		char c;
		bool loaded = false;
		cout << "MENI:" << endl;
		cout << "1. Ucitavanje podataka o mrezi gradskog prevoza" << endl;
		cout << "2. Manipulacije nad mrezom" << endl;
		cout << "3. Generisanje grafovskih fajlova" << endl;
		cout << "0. Kraj programa" << endl;
		cin >> i; cout << endl;
		switch (i) {
		case 1:
			while (true)
			{
				system("cls");
				cout << "1. Grupno citanje podataka" << endl;
				cout << "2. Pojedinacno citanje podataka" << endl;
				cout << "0. Povratak na meni" << endl;
				int i1; cin >> i1;
				switch (i1)
				{
				case 0: system("cls"); break;
				case 1:
					if (loaded == false) {
						network->readAllLines("_lines.txt");
						loaded = true;
						cout << "USPJESNO UCITAVANJE!" << endl;
					}
					else
						cout << "SVE LINIJE SU VEC UCITANE!" << endl;
					break;
				case 2:
					if (loaded == false) {
						cout << "Unesite broj linije: ";
						cin >> s;
						if(network->readLine("_lines.txt", s)==true)
							cout << "USPJESNO UCITAVANJE!" << endl;
						else
							cout << "LINIJA JE VEC UCITANA!" << endl;
					}
					else
						cout << "SVE LINIJE SU VEC UCITANE!" << endl;

					break;
				default:
					cout << "NEISPRAVAN UNOS, MOLIM VAS PONOVITE! " << endl;
					break;
				}
				if (i1 == 0)break;
				std::this_thread::sleep_for(std::chrono::seconds(1));
			}
			break;
		case 2:
			while (true)
			{
				system("cls");
				cout << "1. Osnovne manipulacije nad mrezom" << endl;
				cout << "2. Filtriranje podataka" << endl;
				cout << "3. Dodatne manipulacije nad mrezom" << endl;
				cout << "0. Povratak na meni" << endl;
				int i2; cin >> i2;
				switch (i2) { //OSNOVNE MANIPULACIJE
				case 0:
					system("cls"); break;
				case 1:  
					while (true)
					{
						system("cls");
						cout << "1. Prikaz svih linija" << endl;
						cout << "2. Prikaz svih linije sa njihovim stajalistima" << endl;
						cout << "3. Prikaz odredjenje linije sa njenim stajalistima" << endl;
						cout << "4. Izmjena oznake linije" << endl;
						cout << "5. Brisanje linije" << endl;
						cout << "6. Dodavanje stajalista linije" << endl;
						cout << "7. Brisanje stajalista linije" << endl;
						cout << "0. Povratak na meni" << endl;
						int num, zone,num1; double x, y; string dir;
						int ii1; cin >> ii1;
						switch (ii1) {
						case 0: system("cls"); break;
						case 1:
							if (network->getListLines().size() == 0)
								cout << "NE POSTOJI NIJEDNA LINIJA U MREZI!" << endl;
							else
								for (Line* i : network->getListLines()) cout << *i;
							break;
						case 2:
							if (network->getListLines().size() == 0)
								cout << "NE POSTOJI NIJEDNA LINIJA U MREZI!" << endl;
							else {
								for (Line* i : network->getListLines()) {
									cout << *i << endl;
									cout << i->getDirA() << endl << i->getDirB() << endl << endl;
								}
							}
							break;
						case 3:
							cout << "Unesite oznaku linije: "; cin >> s; 
							if (network->findLine(s) == nullptr) {
								cout << "ZADATA LINIJA NE POSTOJI!" << endl;
								break;
							}
							cout << endl << *network->findLine(s) << endl << network->findLine(s)->getDirA() << endl << network->findLine(s)->getDirB() << endl;
							break;
						case 4:
							cout << "Unesite staru i novu oznaku linije: "; 
							cin >> s >> s1;
							if (network->findLine(s) == nullptr) {
								cout << "ZADATA LINIJA NE POSTOJI!" << endl;
								break;
							}
							network->findLine(s)->setNumber(s1);
							cout << *network->findLine(s1) << endl;
							break;
						case 5:
							cout << "Unesite oznaku linije: ";
							cin >> s;
							cout << endl << "Da li ste sigurni da zelite da obriste liniju?\n1. Da\n2. Ne" << endl;
							cin >> num;
							if (num == 1) {
								if (network->findLine(s) != nullptr) {
									network->deleteLine(network->findLine(s));
									loaded = false;
									cout << "USPJESNO IZBRISANA LINIJA!" << endl;
								}
								else
									cout << "LINJA NE POSTOJI U MREZI!" << endl;
							}
							break;
						case 6:
							cout << "Unesite liniju i smjer za koje dodajete stajaliste: "; cin >> s >> dir;
							if (network->findLine(s) == nullptr) {
								cout << "ZADATA LINIJA NE POSTOJI!" << endl;
								break;
							}
							cout << "Unesite podatke o stajalistu:" << endl << "Sifra: "; cin >> num;
							cout << "Naziv: "; cin >> s1;
							cout << "Koordinate: "; cin >> x >> y;
							cout << "Zona: "; cin >> zone;
							if (dir == "A")
								network->findLine(s)->getDirA().addStop(num, s1, Location(x, y), zone, network->findLine(s));
							else
								network->findLine(s)->getDirB().addStop(num, s1, Location(x, y), zone, network->findLine(s));
							cout << "USPJESNO DODAVANJE!" << endl;
							break;
						case 7:
							cout << "Unesite oznaku linije i stajalista: "; cin >> s >> num;
							cout << endl << "Da li ste sigurni da zelite da obriste stajaliste?\n1. Da\n2. Ne" << endl;
							cin >> num1;
							if (num1 == 1) {
								if (network->findLine(s) == nullptr) {
									cout << "ZADATA LINIJA NE POSTOJI!" << endl;
									break;
								}
								if (network->findLine(s)->deleteStop(num))
									cout << "USPJESNO!" << endl;
								else
									cout << "ZADATO STAJALISTE NE POSTOJI!" << endl;
							}
							break;
						default:cout << "NEISPRAVAN UNOS, MOLIM VAS PONOVITE! " << endl;
							break;
						}
						if (ii1 == 0)break;
						cin >> s;
					}
					break;
				case 2: //FILTRIRANJE PODATAKA
					while (true)
					{
						system("cls");
						cout << "Filtriranje na osnovu zona: " << endl;
						cout << "\t 1. Izbavanje linija iz 4. zone" << endl;
						cout << "\t 2. Izbacivanje linija iz 3. i 4. zone" << endl;
						cout << "\t 3. Izbacivanje linija iz 2. 3. i 4. zone" << endl;
						cout << "Filtriranje na osnovu oznake linja: " << endl;
						cout << "\t 4. Izbaciti sve linije koje imaju oznaku manju od zadate" << endl;
						cout << "\t 5. Izbaciti sve linije koje imaju oznaku vecu od zadate" << endl;
						cout << "\t 6. Izbaciti sve linije koje imaju oznaku van zadatog opsega" << endl;
						cout << "Filtriranje na osnovu broja stajalista linije: " << endl;
						cout << "\t 7. Izbaciti sve linije koje imaju manje stajalista od zadatog broja" << endl;
						cout << "\t 8. Izbaciti sve linije koje imaju vise stajalista od zadatog broja" << endl;
						cout << "0. Povratak na meni" << endl;
						int ii2; cin >> ii2;
						smatch result, result1;
						regex reg("[^0-9]*([0-9]+).*");
						FilterZone f(network); FilterNumLine f1(network); FilterNumStops f2(network);
						int num,num1,num2;
						loaded = false;
						switch (ii2)
						{
						case 0:
							system("cls"); 
							break;
						case 1:
							if (network->getListLines().size() != 0) {
								f.filtring(3);
								cout << "USPJESNO FILTRIRANJE" << endl;
							} break;
						case 2:
							if (network->getListLines().size() != 0) {
								f.filtring(2);
								cout << "USPJESNO FILTRIRANJE" << endl;
							} break;
						case 3:
							if (network->getListLines().size() != 0) {
								f.filtring(1);
								cout << "USPJESNO FILTRIRANJE" << endl;
							} break;
						case 4: 
							cout << "Unesite oznaku: "; cin >> num1;
							if (network->getListLines().size() != 0) {
								f1.filtring(num1, -1);
								cout << "USPJESNO FILTRIRANJE" << endl;
							} break;
						case 5:
							cout << "Unesite oznaku: "; cin >> num1;
							if (network->getListLines().size() != 0) {
								f1.filtring(num1, -2);
								cout << "USPJESNO FILTRIRANJE" << endl;
							} break;
						case 6:
							cout << "Unesite dvije oznaku: "; cin >> num1 >> num2;
							if (network->getListLines().size() != 0) {
								f1.filtring(num1, num2);
								cout << "USPJESNO FILTRIRANJE" << endl;
							}  break;
						case 7:
							cout << "Unesite broj: " << endl; cin >> num;
							if (network->getListLines().size() != 0) {
								f2.filtring(num, -1);
								cout << "USPJESNO FILTRIRANJE" << endl;
							} break;
						case 8:
							cout << "Unesite broj: " << endl; cin >> num;
							if (network->getListLines().size() != 0) {
								f2.filtring(num, -2);
								cout << "USPJESNO FILTRIRANJE" << endl;
							} break;
						default:cout << "NEISPRAVAN UNOS, MOLIM VAS PONOVITE! " << endl;
							break;
						}
						if (ii2 == 0)break;
						std::this_thread::sleep_for(std::chrono::seconds(1));
					}
					break;
				case 3: //DODATNE MANIPULACIJE
					while (true)
					{
						system("cls");
						cout << "1. Odredjivanje skupa linija sa kojima data linija ima zajednicka stajalista" << endl;
						cout << "2. Odredjivanje da li data linija prolazi kroz zadata dva stajalista u istom smjeru svog kretanja" << endl;
						cout << "3. Odredjivanje linije sa kojom data linija ima najvise zajednickih stajalista" << endl;
						cout << "4. Odredjivanje najblizeg stajalista u odnosu na zadatu geografsku lokaciju" << endl;
						cout << "5. Odredjivanje broja zajednickih stajalista za sve parove linija koje imaju zajednicko stajaliste" << endl;
						cout << "6. Odredjivanje svih linija koje prolaze kroz dato stajaliste" << endl;
						cout << "7. Odredjivanje svih stajalista do kojih je moguce stici iz stajalista uz voznju maksimalno jednu stanicu" << endl;
						cout << "8. Odredjivanje najmanjeg potrebnog broja presjedanja na putu izmedju dva stajalista " << endl;
						cout << "9. Odredjivanje najkraceg puta izmedju dva stajalista" << endl;
						cout << "0. Povratak na meni" << endl;
						int ii3; cin >> ii3;
						int n1, n2, n3;
						double x, y;
						switch (ii3)
						{
						case 0:
							system("cls");
							break;
						case 1:
							cout << "Unesite oznaku linije: "; cin >> s; cout << endl;
							if (network->findLine(s) != nullptr)
								for (Line* i : network->findLine(s)->commonStopsWithLines()) cout << *i;
							else
								cout << "ZADATA LINIJA NE POSTOJI!" << endl;
							break;
						case 2:
							cout << "Unesite oznaku linije: "; cin >> s;
							cout << "Unesite oznake dva stajalista: "; cin >> n1 >> n2;
							if (network->findLine(s) == nullptr) {
								cout << "ZADATA LINIJA NE POSTOJI!" << endl; break;
							}
							if (network->findLine(s)->through2stops(n1, n2))
								cout << "LINIJA PROLAZI KROZ ZADATA STAJALISTA!" << endl;
							else
								cout << "LINIJA NE PROLAZI KROZ ZADATA STAJALISTA!" << endl;
							break;
						case 3:
							cout << "Unesite oznaku linije: "; cin >> s;
							if (network->findLine(s) == nullptr) {
								cout << "ZADATA LINIJA NE POSTOJI!" << endl; break;
							}
							cout <<endl<< network->findLine(s)->theMostCommonStops().first;
							cout << "Broj zajednickih stajalista: " << network->findLine(s)->theMostCommonStops().second << endl;
							break;
						case 4:
							cout << "Unesite koordinate (x,y): "; cin >> x >> y;
							cout << "Da li zelite da nadjete najblize stajaliste za odredjenu liniju: " << endl << "1. Da" << endl << "2. Ne" << endl;
							cin >> n1; 
							if (n1 == 1) { 
								cout << "Unesite oznaku linije: "; cin >> s; 
								if (network->findLine(s) == nullptr) {
									cout << "ZADATA LINIJA NE POSTOJI!" << endl; break;
								}
								cout<<* network->nearestStop(Location(x, y),network->findLine(s));
							}
							else
								if(network->getListLines().size()!=0)
									cout<<* network->nearestStop(Location(x, y));
							break;
						case 5:
							cout << "Da li zelite da zadate kriterijum za minimalan broj zajednickih stajalista?" << endl << "1. Da\n" << "2. Ne\n";
							cin >> n1; 
							if (n1 == 2) {
								for (auto i : network->allPairWithCommonStops())
									cout << i.first.first << i.first.second << "Broj zajednickih stjalista:  " << i.second << endl<<endl;
							}
							else
							{
								cout << "Unesite minimalan broj zajednickih stajalista: "; cin >> n2;
								for (auto i : network->allPairWithCommonStops(n2))
									cout << i.first.first << i.first.second << "Broj zajednickih stjalista:  " << i.second << endl << endl;
							}
							break;
						case 6:
							cout << "Unesite broj stajalista: "; cin >> n1; cout << endl;
							if (Direction::getMap().find(n1) == Direction::getMap().end()) {
								cout << "ZADATO STAJALISTE NE POSTOJI!" << endl; break;
							}
							for (Line * i : Direction::getMap()[n1]->linesThroughStop()) cout << *i;
							break;
						case 7:
							cout << "Unesite broj stajalista: "; cin >> n1; cout << endl;
							if (Direction::getMap().find(n1) == Direction::getMap().end()) {
							cout << "ZADATO STAJALISTE NE POSTOJI!" << endl; break;
							}
							for (BusStop* i : Direction::getMap()[n1]->drive1stop()) cout << *i;
							break;
						case 8: 
							cout << "Unesite brojeve dva stajalista: "; cin >> n1 >> n2; cout << endl;
							if (Direction::getMap().find(n1) == Direction::getMap().end()) {
								cout << "PRVO STAJALISTE NE POSTOJI!" << endl; break;
							}
							if (Direction::getMap().find(n2) == Direction::getMap().end()) {
								cout << "DRUGO STAJALISTE NE POSTOJI!" << endl; break;
							}
							n3 = BusStop::minNumOfTransfer(Direction::getMap()[n1], Direction::getMap()[n2]);
							if (n3 != -1)cout << "Minimalan broj presjedanja je: " << n3 << endl;
							else cout << "NE POSTOJI PUTANJA IZMEDJU ZADATIH STAJALISTA!" << endl; 
							break;
						case 9:
							cout << "Unesite brojeve dva stajalista: "; cin >> n1 >> n2; cout << endl;
							if (Direction::getMap().find(n1) == Direction::getMap().end()) {
								cout << "PRVO STAJALISTE NE POSTOJI!" << endl; break;
							}
							if (Direction::getMap().find(n2) == Direction::getMap().end()) {
								cout << "DRUGO STAJALISTE NE POSTOJI!" << endl; break;
							}
							if (BusStop::shortestPath(Direction::getMap()[n1], Direction::getMap()[n2]).empty() == true)
								cout << "NE POSTOJI PUT IZMEDJU ZADATIH STAJALISTA!" << endl;
							for (BusStop* i : BusStop::shortestPath(Direction::getMap()[n1], Direction::getMap()[n2])) cout << *i;
							break;
						default:
							cout << "NEISPRAVAN UNOS, MOLIM VAS PONOVITE! " << endl;
							break;
						}
						if (ii3 == 0)break;
						cin >> c;
					}
					break;
				default:
					cout << "NEISPRAVAN UNOS, MOLIM VAS PONOVITE! " << endl;
					break;
				}
				if (i2 == 0)break;
			}
			break;
		case 3: //GENERISANJE GRAFOVSKIH FAJLOVA
			while (true)
			{
				system("cls");
				cout << "Izaberite jedan od sledeca dva modela: " << endl;
				cout << "1. L-model" << endl << "2. C-model" << endl;
				cout << "0. Nazad na meni\n";
				int i3; cin >> i3;
				switch (i3)
				{
				case 0: 
					system("cls");
					break;
				case 1:
					gL->generate(); 
					cout << "USPJESNO GENERISANJE" << endl;
					break;
				case 2:
					gC->generate(); 
					cout << "USPJESNO GENERISANJE" << endl;
					break;
				default:
					cout << "NEISPRAVAN UNOS, MOLIM VAS PONOVITE! " << endl;
					break;
				}
				if (i3 == 0)break;
				std::this_thread::sleep_for(std::chrono::seconds(1));

				cout << endl << "Izaberite format: " << endl;
				cout << "1. GML" << endl << "2. CSV - grane" << endl << "3. CSV - liste susjednosti" << endl;
				int ii3; 
				cin >> ii3;
				switch (ii3)
				{
				case 1:
					cout << "Unesite ime izlazne datoteke: "; cin >> s;
					if (i3 == 2) 
						f = new GMLFormat(s + ".gml", gC); 
					else
						f = new GMLFormat(s + ".gml", gL);
					f->formating();
					break;
				case 2:
					cout << "Unesite ime izlazne datoteke: "; cin >> s;
					if (i3 == 2)
						f = new CSV_Edges(s + ".csv", gC);
					else
						f = new CSV_Edges(s + ".csv", gL);
					f->formating();
					break;
				case 3:
					cout << "Unesite ime izlazne datoteke: "; cin >> s;
					if (i3 == 2)
						f = new CSV_NeighLists(s + ".csv", gC);
					else
						f = new CSV_NeighLists(s + ".csv", gL);
					f->formating();
					break;
				default:
					cout << "NEISPRAVAN UNOS, MOLIM VAS PONOVITE! " << endl;
					break;
				}
				std::this_thread::sleep_for(std::chrono::seconds(1));
			}
			break;
		case 0:
			system("cls");
			exit(-1);
		default:
			cout << "NEISPRAVAN UNOS, MOLIM VAS PONOVITE! " << endl;
			break;
		}
	}
	delete network;
	delete gC;
	delete gL;
	if( f!= nullptr) delete f;
}