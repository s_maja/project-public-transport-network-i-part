#ifndef  _FILTER_NUM_LINE_H_
#define  _FILTER_NUM_LINE_H_

#include "Filter.h"

class FilterNumLine : public Filter{
public:
	FilterNumLine(Network* n) : Filter(n) {}

	void filtring(int from, int to) {
		for (auto i = network->getListLines().begin(); i != network->getListLines().end();) {
			regex reg("[^0-9]*([0-9]+).*");
			smatch result;
			string s = (*i)->getNumber();
			if (regex_match(s, result, reg)) {
				int num = atoi(result.str(1).c_str());
				if ((to == -1) && (num <= from)) { //less=-1 delete all lines which have number less than num
					auto &temp = i++;
					network->deleteLine((*temp));
					continue;
				}
				if ((to == -2) && (num >= from)) {	//less=-2 delete all lines which have number greater than num
					auto &temp = i++;
					network->deleteLine((*temp));
					continue;
				}	
				if (((num < from) || (num > to)) && (to!= -2) && (to!=-1)) {
					auto &temp = i++;
					network->deleteLine((*temp));
					continue;
				}
			}
			i++;
		}
	}
};
#endif // ! _FILTER_NUM_LINE_H_
