#ifndef _CGENERATOR_H_
#define _CGENERATOR_H_

#include "Generator.h"

class CGenerator : public Generator {
	set<pair<Line, Line>> edges;
	set<Line*> nodes;
public:
	CGenerator(Network * n) : Generator(n) { }
	char getMark() override { return 'C'; }

	set<Line*>& getNodes() { return nodes; }   //C_Generator
	set<pair<Line, Line>>& getEdges() { return edges; }
	set<BusStop*>& getNodes(int) { set<BusStop*> s; return s; }  // L_genrator
	set<pair<BusStop*, BusStop*>>& getEdges(int) { set<pair<BusStop*, BusStop*>> s; return s; }

	void generate()override {
		for (Line* i : network->getListLines()) 
			nodes.insert(i);  //nodes are lines in this(C) type of graph

		map<pair<Line, Line>, int> m = network->allPairWithCommonStops();
		for (auto i : m)    //edges are pair of all lines which have common stop(s)
			edges.insert(i.first);
	}
};
#endif // !_CGENERATOR_H_