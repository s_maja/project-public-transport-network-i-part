#ifndef _LGENERATOR_H_
#define _LGENERATOR_H_

#include "Generator.h"

class LGenerator : public Generator {
	set<pair<BusStop*, BusStop*>> edges;
	set<BusStop*> nodes;
public:
	LGenerator(Network * n) : Generator(n) { }
	char getMark() override { return 'L'; }

	set<BusStop*>& getNodes(int i) { return nodes; }  //L_generator
	set<pair<BusStop*, BusStop*>>& getEdges(int i) { return edges; }
	set<Line*>& getNodes() { set<Line*> s;  return s; }  //C_generator
	set<pair<Line, Line>>& getEdges() { set<pair<Line, Line>> s; return s; }
	

	void generate()override {
		map<int, int> m;
		
		for (auto& i : network->getListLines()) {
			char c = 'A';
			while (c != 'C') {
				Direction &d = i->getDir(c);
				for (auto& k : d.getBusStops()) {
					if (m[k->getNumber()]!= 1) {
						nodes.insert(k);
						m[k->getNumber()] = 1;
						set<BusStop*>s = k->drive1stop();
						for (BusStop* j : s) {   //edges are exists if from one stop we can go to another driveing just 1 stop
							pair<BusStop*, BusStop*>p = make_pair(k, j);
							if (edges.find(p) == edges.end())
								edges.insert(p);
						}
					}
				}
				c++;
			} //end while
		} //end for

		//for (auto& i : nodes) cout << *i;
	}
};
#endif // !_LGENERATOR_H_