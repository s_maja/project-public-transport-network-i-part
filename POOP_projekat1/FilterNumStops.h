#ifndef  _FILTER_NUM_STOPS_H_
#define _FILTER_NUM_STOPS_H_

#include "Filter.h"

class FilterNumStops : public Filter {
public:
	FilterNumStops(Network* n) : Filter(n) {}

	void filtring(int num, int less) { 
		
		for (auto i = network->getListLines().begin(); i != network->getListLines().end();) {
			int sum = (*i)->getDirA().getBusStops().size();
			sum += (*i)->getDirB().getBusStops().size();
			if ((less == -1) && (sum < num)) { //less=-1 delete all lines which have less than num stops
				auto& temp = i++;
				network->deleteLine((*temp));
				continue;
			}
			if ((less == -2) && (sum >= num)) {//less=-2 delete all lines which have more than num stops
				auto& temp = i++;
				network->deleteLine((*temp));
				continue;
			}
			i++;
		}
	}
};

#endif // ! _FILTER_NUM_STOPS_
