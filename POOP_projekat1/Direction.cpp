#include "Direction.h"

map <int, BusStop*> Direction::mapOfStops = {{ 0, nullptr }};

void Direction::readAllStops(string fileName, Line *l)
{
	ifstream file(fileName);
	string s;

	regex reg("([^!]*)!([^!]*)!([^!]*)!([^!]*)!([0-9]*).*");


	while (getline(file, s)) {
		smatch result;
		if (regex_match(s, result, reg)) {

			int num = atoi(result.str(1).c_str()); //number of stop
			string name = result.str(2);  //name of stop
			double x = atof(result.str(3).c_str());	//x coordinat
			double y = atof(result.str(4).c_str()); //y coordinat
			int zone = atoi(result.str(5).c_str()); //number zone	

			insertStop(BusStop(num, name, Location(x,y), zone),l);
		}
		else
		{
			cout << "No match" << endl;
		}
	}

	file.close();
}

void Direction::insertStop(BusStop bs, Line* l)
{	
	if (findStop(bs.getNumber()) == nullptr) {
		auto iter = mapOfStops.find(bs.getNumber());

		if (iter == mapOfStops.end()) {
			BusStop *newBS = new BusStop(bs.getNumber(), bs.getName(), bs.getLocation(), bs.getZone());
			pair<int, BusStop*> p = make_pair(bs.getNumber(), newBS);
			mapOfStops.insert(p);//if not exist in map, add it
			busStops.push_back(newBS); // add BusStop in list of busStops
		}
		else
			busStops.push_back(iter->second);    // add BusStop in list of busStops

		mapOfStops[bs.getNumber()]->insertLineInSet(l); // add Line in set of this BusStop
	}
}

BusStop* Direction::findStop(int num) {
	for (auto i : busStops) {
		if (i->getNumber() == num)
			return i;
	}
	return nullptr;
}

BusStop* Direction::findNextStop(int num) {
	bool boo = false;
	for (auto& i : busStops) {
		if (boo == true) return i;
		if (i->getNumber() == num) boo = true; 
	}
	return nullptr;
}

ostream& operator<<(ostream & ot, const Direction& dir)
{
	for (BusStop* i : dir.busStops) {
		ot << *i;
	}
	return ot;
}

void Direction::addStop(int num, string n, Location loc, int z, Line * l) {
	insertStop(BusStop(num, n, loc, z),l);
}

void Direction::deleteLineFromStops(Line *l) {
	for (auto i = busStops.begin(); i != busStops.end();) {
		(*i)->deleteLineFromSet(l);
		i++;
	}
}

void Direction::deleteBusStop(int stop) {
	for (auto& it = busStops.begin(); it != busStops.end();) {
		if (stop == (*it)->getNumber()) {
			*it = nullptr;
			busStops.erase(it);
			break;
		}
		else
			it++;
	}
}

Direction::~Direction() {
	for (auto iter = busStops.begin(); iter != busStops.end();){
		BusStop *s = *iter;
		auto& oldIter = iter++;
		mapOfStops.erase((*oldIter)->getNumber());
		busStops.erase(oldIter);
	}
}