#include "Line.h"

ostream& operator<<(ostream& ot, const Line & line)
{
	ot << line.getNumber() << " " << line.getFirstStop() << " - " << line.getLastStop() << "\n";
	return ot;
}

bool Line::through2stops(int s1, int s2)
{

	int dir = 1;
	while (dir != -1) {
		Direction d = dir ? dirA : dirB;
		bool b1 = false, b2 = false;
		for (auto i : d.getBusStops()) {
			if (i->getNumber() == s1) b1 = true;
			if (i->getNumber() == s2) b2 = true;
			if (b1 && b2) return true;
		}
	  dir--;
	}
	return false;
}

set<Line*> Line::commonStopsWithLines()
{
	set<Line*> lin;  
	int dir = 1;
	while (dir != -1) {
		Direction d = dir ? dirA : dirB;
		for (auto& i : d.getBusStops()) {
			for (auto j : i->getSetLines()) {
				if (j == this) continue;
				if ((lin.find(j) == lin.end()))
					lin.insert(j);  
			}
		}
		dir--;
	}
	return lin;
}

map<Line, int> Line::numCommonStopsWithLines() {
	map<Line, int> lin;  
	int dir = 1;
	while (dir != -1) {
		Direction d = dir ? dirA : dirB;
		for (auto& i : d.getBusStops()) {
			for (auto j : i->getSetLines()) {
				if (j == this) continue;
				if ((lin.find(*j) == lin.end()))
					lin[*j]=1;  
				else
					lin[*j]=lin[*j]+1;
			}
		}
		dir--;
	}
	return lin;
}

pair<Line, int> Line::theMostCommonStops() {  //PROVJERI DA LI TREBAS OBA SMJERA PROCI
	map<Line, int> m;   
	int dir = 0;
	while (dir != 2) {
		Direction dd;
		dir ? (dd = dirA) : (dd = dirB);
		for (BusStop* i : dd.getBusStops()) {
			for (Line * j : i->getSetLines()) {
				if (j->getNumber() == this->getNumber())
					continue;
				auto iter = m.find(*j);
				if (iter == m.end())
					m[*j] = 1;
				else
					m[*j] = m[*j] + 1;
			}
		}
		dir++;
	}
	using pt = pair<Line, int>;
	return *max_element(m.begin(), m.end(), [](pt a, pt b)->bool {return a.second < b.second; });
}

bool  Line::deleteStop(int stop) {
	char dir = 'A';
	
	while (dir != 2) {
		Direction &d = getDir(dir);
		auto iter = find_if(d.getBusStops().begin(), d.getBusStops().end(), [stop](BusStop *b)->bool {return stop == b->getNumber(); });
		if (iter != d.getBusStops().end()) {
			(*iter)->deleteLineFromSet(this);
			d.deleteBusStop(stop);
			return true;
		}
		dir++;
	}
	return false;
}
