#ifndef _FILTER_H_
#define _FILTER_H_

#include "Network.h"

class Filter {
protected:
	Network *network;
public:
	Filter(Network *n) : network(n) {}
	virtual ~Filter(){}

	Network* getNetwork() { return network; }
	const Network* getNetwork() const { return network; }
	void serNetwork(Network *n) { network = n; }
	virtual void filtring(int parametar1 ,int parametar2 =-1) = 0;
};
#endif // !_FILTER_H_
