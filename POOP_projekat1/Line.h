#ifndef _LINE_H
#define _LINE_H

#include "Direction.h"

class Line {
private:
	Direction dirA, dirB;
	string number;
	string firstStop, lastStop;
public:
	Line(string num, string f, string l):number(num), firstStop(f), lastStop(l) {}

	void setNumber(string i) { number = i; }
	const Direction& getDirA() const { return dirA;  }
	const Direction& getDirB() const { return dirB;  }
	const Direction& getDir(char c)const { if (c == 'A') return dirA; else dirB; }
	Direction& getDir(char c) { if (c == 'A') return dirA; else return dirB; }
	Direction& getDirA()  { return dirA; }
	Direction& getDirB()  { return dirB; }
	string getNumber() const { return number; }
	string getFirstStop() const { return firstStop; }
	string getLastStop() const { return lastStop;  }

	bool deleteStop(int);
	set<Line*> commonStopsWithLines();  //find other lines which have common stops with given line (regardless of direction)
	map<Line, int> numCommonStopsWithLines(); // find other lines which have common stops, and num of common stops, with given line(regardless of direction)
	bool through2stops (int, int);  //lines is going trought 2 stops in same direction?
	pair<Line, int> theMostCommonStops(); //find line which have most common stops with given one

	friend bool operator<(const Line &l1, const Line &l2) { return l1.number < l2.number; }
	friend ostream& operator<<(ostream& ot, const Line& line);
};
#endif // !_LINE_H
