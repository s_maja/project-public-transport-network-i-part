#include "GMLFormat.h"

void GMLFormat::filtringL() {
	ofstream myfile;
	myfile.open(fileName);
	myfile << "graph" << endl << "[" << endl;
	for (BusStop* i : gen->getNodes(1))
		myfile << "node" << endl << "[" << endl << " id " << i->getNumber() << endl << " label " << "\"Node " << i->getNumber() << "\"" << endl << "]" << endl;
	
	for (pair<BusStop*, BusStop*> i : gen->getEdges(1))
		myfile << "edge" << endl << "[" << endl<< " source " << i.first->getNumber() << endl << " target " << i.second->getNumber() << endl
				 << " label " << "\"Edge " << i.first->getNumber()<<" to " <<i.second->getNumber() << "\"" << endl << "]" << endl;
	myfile << "]";
	myfile.close();
}

void GMLFormat::filtringC() {
	ofstream myfile;
	myfile.open(fileName);
	myfile << "graph" << endl << "[" << endl;
	for (Line* i : gen->getNodes())
		myfile << "node"<<endl<<"[" << endl << " id " << i->getNumber() << endl << " label " << "\"Node " + i->getNumber() << "\"" << endl << "]" << endl;

	for (pair<Line, Line> i : gen->getEdges())
		myfile << "edge" << endl << "[" << endl << " source " << i.first.getNumber() << endl << " target " << i.second.getNumber() << endl
		<< " label " << "\"Edge" + i.first.getNumber() << " to " + i.second.getNumber() << "\"" << endl << "]" << endl;
	myfile << "]";
	myfile.close();
}