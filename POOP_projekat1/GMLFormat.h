#ifndef _GMLFORMAT_H_
#define _GMLFORMAT_H_

#include "Format.h"

class GMLFormat : public Format {
public:
	GMLFormat() = default;
	GMLFormat(string name,  Generator* g) : Format(name, g) {  }

	void filtringL();
	void filtringC();
	
	 void formating() override {
		(gen->getMark()=='L') ? filtringL() : filtringC();
	}
};
#endif // !_GMLFORMAT_H_