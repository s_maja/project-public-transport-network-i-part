#include "CSV_Edges.h"

void CSV_Edges::filtringL() {
	ofstream myfile;
	myfile.open(fileName);
	for (pair<BusStop*, BusStop*> i : gen->getEdges(1))
		myfile << i.first->getNumber() << ";" << i.second->getNumber() << endl;
	myfile.close();
}
void CSV_Edges::filtringC() {
	ofstream myfile;
	myfile.open(fileName);
	for (pair<Line, Line> i : gen->getEdges())
		myfile << i.first.getNumber() << ";" << i.second.getNumber() << endl;
	myfile.close();
}