#ifndef _BUSSTOP_H
#define _BUSSTOP_H

#include "Location.h"
#include <string>
#include <cstdlib>
#include <set>
#include <list>
#include <fstream>
#include <regex>
#include <map>
#include <vector>

class Line;

class BusStop {
private:
	struct Elem {
	BusStop* stop;
	vector<BusStop*> path;
	Elem(BusStop* b, vector<BusStop*> v) :stop(b), path(v) {}
	bool operator<(Elem e) const { return e.stop->getNumber() < this->stop->getNumber(); }
	};

	set<Line*> setLines;
	Location location;
	int number;
	string name;
	int zone;
	static bool insertInSet(set<Elem>&, set<Elem>&, BusStop*, BusStop*,map<int,int>&);
public:
	BusStop() = default;
	BusStop(int num, string n, Location l, int z) :name(n), location(l) {
		number = num;
		zone = z;
	}

	friend class Direction;
	friend class Line;
	const Location& getLocation() const { return location; }
	Location& getLocation() { return location; }
	int getNumber() const { return number; }
	string getName() const { return name; }
	int getZone() const { return zone; }
	const set<Line*> getSetLines() const { return setLines; }
	set<Line*> getSetLines()  { return setLines; }

	set<BusStop*> drive1stop(); //find all stop where i can go from current stop driving just 1 stop
	static int minNumOfTransfer(BusStop*, BusStop*); // find minimal number of transfer(presjedanja) between two stops
	static vector<BusStop*> shortestPath(BusStop*, BusStop*); //find shortest path between two stops( counting number of stops between them)
	void insertLineInSet(Line *l);
	set<Line*> linesThroughStop();	 //find allLines which goes through stop
	void deleteLineFromSet(Line*);

	friend ostream& operator<<(ostream& ot, const BusStop& bs) {
		return ot << bs.number << " " << bs.name << " " << bs.location << " " << bs.zone << "\n";
	}
};
#endif // !_BUSSTOP_H
